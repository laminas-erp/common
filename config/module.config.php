<?php

namespace Lerp\Common;

use Bitkorn\User\Entity\User\Rightsnroles;
use Lerp\Common\Controller\Ajax\ConfigController;
use Lerp\Common\Controller\Ajax\Lists\CostCentreController;
use Lerp\Common\Controller\Ajax\Lists\LanguageController;
use Lerp\Common\Controller\Ajax\Lists\PayTermController;
use Lerp\Common\Controller\Ajax\Lists\ProjectTypeController;
use Lerp\Common\Controller\Ajax\Lists\Skr03Controller;
use Lerp\Common\Controller\Rest\CountryListController;
use Lerp\Common\Controller\Ajax\Lists\IndustryCategoryController;
use Lerp\Common\Controller\Ajax\Lists\QuantityUnitController;
use Lerp\Common\Factory\Controller\Ajax\ConfigControllerFactory;
use Lerp\Common\Factory\Controller\Ajax\Lists\CostCentreControllerFactory;
use Lerp\Common\Factory\Controller\Ajax\Lists\LanguageControllerFactory;
use Lerp\Common\Factory\Controller\Ajax\Lists\PayTermControllerFactory;
use Lerp\Common\Factory\Controller\Ajax\Lists\ProjectTypeControllerFactory;
use Lerp\Common\Factory\Controller\Ajax\Lists\Skr03ControllerFactory;
use Lerp\Common\Factory\Controller\Rest\CountryListControllerFactory;
use Lerp\Common\Factory\Controller\Ajax\Lists\IndustryCategoryControllerFactory;
use Lerp\Common\Factory\Controller\Ajax\Lists\QuantityUnitControllerFactory;
use Lerp\Common\Factory\Service\CommonGodServiceFactory;
use Lerp\Common\Factory\Service\ConfigServiceFactory;
use Lerp\Common\Factory\Service\Lists\CostCentreServiceFactory;
use Lerp\Common\Factory\Service\Lists\CountryServiceFactory;
use Lerp\Common\Factory\Service\Lists\IndustryCategoryServiceFactory;
use Lerp\Common\Factory\Service\Lists\PayTermServiceFactory;
use Lerp\Common\Factory\Service\Lists\ProjectTypeServiceFactory;
use Lerp\Common\Factory\Service\Lists\QuantityUnitServiceFactory;
use Lerp\Common\Factory\Service\Lists\Skr03ServiceFactory;
use Lerp\Common\Factory\Table\ConfigTableFactory;
use Lerp\Common\Factory\Table\Lists\CostCentreTableFactory;
use Lerp\Common\Factory\Table\Lists\CountryTableFactory;
use Lerp\Common\Factory\Table\Lists\IndustryCategoryTableFactory;
use Lerp\Common\Factory\Table\Lists\PayTermTableFactory;
use Lerp\Common\Factory\Table\Lists\ProjectTypeTableFactory;
use Lerp\Common\Factory\Table\Lists\QuantityUnitTableFactory;
use Lerp\Common\Factory\Table\Lists\Skr03TableFactory;
use Lerp\Common\Service\CommonGodService;
use Lerp\Common\Service\ConfigService;
use Lerp\Common\Service\Lists\CostCentreService;
use Lerp\Common\Service\Lists\CountryService;
use Lerp\Common\Service\Lists\IndustryCategoryService;
use Lerp\Common\Service\Lists\PayTermService;
use Lerp\Common\Service\Lists\ProjectTypeService;
use Lerp\Common\Service\Lists\QuantityUnitService;
use Lerp\Common\Service\Lists\Skr03Service;
use Lerp\Common\Table\ConfigTable;
use Lerp\Common\Table\Lists\CostCentreTable;
use Lerp\Common\Table\Lists\CountryTable;
use Lerp\Common\Table\Lists\IndustryCategoryTable;
use Lerp\Common\Table\Lists\PayTermTable;
use Lerp\Common\Table\Lists\ProjectTypeTable;
use Lerp\Common\Table\Lists\QuantityUnitTable;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use Lerp\Common\Table\Lists\Skr03Table;

return [
    'router'             => [
        'routes' => [
            /*
             * AJAX
             */
            'lerp_common_ajax_config_valuebykey'                => [
                'type'         => Segment::class,
                'options'      => [
                    'route'       => '/lerp-common-config-value-by-key[/:key]',
                    'constraints' => [
                        'key' => '[0-9A-Za-z_-]*',
                    ],
                    'defaults'    => [
                        'controller' => ConfigController::class,
                        'action'     => 'valueByKey'
                    ],
                ],
                'rightsnroles' => [
                    Rightsnroles::KEY_ROLEMIN => 4,
                ]
            ],
            'lerp_common_ajax_config_configs'                   => [
                'type'         => Literal::class,
                'options'      => [
                    'route'    => '/lerp-common-config-configs',
                    'defaults' => [
                        'controller' => ConfigController::class,
                        'action'     => 'configs'
                    ],
                ],
                'rightsnroles' => [
                    Rightsnroles::KEY_ROLEMIN => 4,
                ]
            ],
            'lerp_common_ajax_config_updateconfig'              => [
                'type'         => Literal::class,
                'options'      => [
                    'route'    => '/lerp-common-config-update-value',
                    'defaults' => [
                        'controller' => ConfigController::class,
                        'action'     => 'updateConfigValue'
                    ],
                ],
                'rightsnroles' => [
                    Rightsnroles::KEY_ROLEMIN => 3,
                ]
            ],
            /*
             * Ajax Lists
             */
            'lerp_common_ajax_lists_industrycategory_uuidassoc' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-common-lists-industry-category',
                    'defaults' => [
                        'controller' => IndustryCategoryController::class,
                        'action'     => 'uuidAssoc'
                    ],
                ],
                'rightsnroles' => [
                    Rightsnroles::KEY_DESC => 'Get the list (uuid assoc) of industry categories',
                    Rightsnroles::KEY_ROLEMIN => 5,
                ]
            ],
            'lerp_common_ajax_lists_quatityunit_uuidassoc'      => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-common-lists-quantityunit[/:resol_group]',
                    'constraints' => [
                        'resol_group' => '[0-9A-Za-z_-]*',
                    ],
                    'defaults'    => [
                        'controller' => QuantityUnitController::class,
                        'action'     => 'uuidAssoc'
                    ],
                ],
                'rightsnroles' => [
                    Rightsnroles::KEY_DESC => 'Get the list (uuid assoc) of quantity units',
                    Rightsnroles::KEY_ROLEMIN => 5,
                ]
            ],
            'lerp_common_ajax_lists_quatityunit_all'      => [
                'type'    => Literal::class,
                'options' => [
                    'route'       => '/lerp-common-lists-quantityunits',
                    'defaults'    => [
                        'controller' => QuantityUnitController::class,
                        'action'     => 'all'
                    ],
                ],
                'rightsnroles' => [
                    Rightsnroles::KEY_DESC => 'Get the list (uuid assoc) of quantity units',
                    Rightsnroles::KEY_ROLEMIN => 5,
                ]
            ],
            'lerp_common_ajax_lists_costcentre_idassoc'         => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-common-lists-costcentre',
                    'defaults' => [
                        'controller' => CostCentreController::class,
                        'action'     => 'idAssoc'
                    ],
                ],
            ],
            'lerp_common_ajax_lists_skr03'                      => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-common-lists-skr03',
                    'defaults' => [
                        'controller' => Skr03Controller::class,
                        'action'     => 'skr03s'
                    ],
                ],
            ],
            'lerp_common_ajax_lists_payterm'                    => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-common-lists-payterm',
                    'defaults' => [
                        'controller' => PayTermController::class,
                        'action'     => 'payTerms'
                    ],
                ],
            ],
            'lerp_common_ajax_lists_projecttype'                => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-common-lists-project-types',
                    'defaults' => [
                        'controller' => ProjectTypeController::class,
                        'action'     => 'projectTypes'
                    ],
                ],
            ],
            'lerp_common_ajax_lists_langisos'                   => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-common-lists-lang-isos',
                    'defaults' => [
                        'controller' => LanguageController::class,
                        'action'     => 'langIsos'
                    ],
                ],
            ],
            /*
             * Rest
             */
            'lerp_common_rest_countrylist'                      => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-common-rest-countrylist[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => CountryListController::class
                    ],
                ],
            ],
        ],
    ],
    'controllers'        => [
        'factories' => [
            // REST
            CountryListController::class      => CountryListControllerFactory::class,
            // AJAX
            ConfigController::class           => ConfigControllerFactory::class,
            // Ajax Lists
            IndustryCategoryController::class => IndustryCategoryControllerFactory::class,
            QuantityUnitController::class     => QuantityUnitControllerFactory::class,
            CostCentreController::class       => CostCentreControllerFactory::class,
            Skr03Controller::class            => Skr03ControllerFactory::class,
            PayTermController::class          => PayTermControllerFactory::class,
            ProjectTypeController::class      => ProjectTypeControllerFactory::class,
            LanguageController::class         => LanguageControllerFactory::class,
        ],
    ],
    'service_manager'    => [
        'factories' => [
            // table
            ConfigTable::class             => ConfigTableFactory::class,
            QuantityUnitTable::class       => QuantityUnitTableFactory::class,
            CountryTable::class            => CountryTableFactory::class,
            IndustryCategoryTable::class   => IndustryCategoryTableFactory::class,
            CostCentreTable::class         => CostCentreTableFactory::class,
            Skr03Table::class              => Skr03TableFactory::class,
            PayTermTable::class            => PayTermTableFactory::class,
            ProjectTypeTable::class        => ProjectTypeTableFactory::class,
            // Service
            CommonGodService::class        => CommonGodServiceFactory::class,
            ConfigService::class           => ConfigServiceFactory::class,
            QuantityUnitService::class     => QuantityUnitServiceFactory::class,
            CountryService::class          => CountryServiceFactory::class,
            IndustryCategoryService::class => IndustryCategoryServiceFactory::class,
            CostCentreService::class       => CostCentreServiceFactory::class,
            Skr03Service::class            => Skr03ServiceFactory::class,
            PayTermService::class          => PayTermServiceFactory::class,
            ProjectTypeService::class      => ProjectTypeServiceFactory::class,
        ],
    ],
    'view_helper_config' => [
        'factories' => [],
    ],
];
