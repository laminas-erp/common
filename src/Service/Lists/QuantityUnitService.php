<?php

namespace Lerp\Common\Service\Lists;

use Bitkorn\Trinket\Service\AbstractService;
use Lerp\Common\Table\Lists\QuantityUnitTable;

class QuantityUnitService extends AbstractService
{
    protected QuantityUnitTable $quantityUnitTable;

    public function setQuantityUnitTable(QuantityUnitTable $quantityUnitTable): void
    {
        $this->quantityUnitTable = $quantityUnitTable;
    }

    /**
     * @param string $quantityunitUuid
     * @return array
     */
    public function getQuantityUnit(string $quantityunitUuid): array
    {
        return $this->quantityUnitTable->getQuantityUnit($quantityunitUuid);
    }

    /**
     * @return array
     */
    public function getQuantityUnits(): array
    {
        return $this->quantityUnitTable->getQuantityUnits();
    }

    /**
     * @param string $resolutionGroup
     * @return array
     */
    public function getQuantityUnitUuidAssoc(string $resolutionGroup = ''): array
    {
        return $this->quantityUnitTable->getQuantityUnitUuidAssoc($resolutionGroup);
    }

    public function getQuantityUnitBaseResolution(string $quantityunitUuid): array
    {
        return $this->quantityUnitTable->getQuantityUnitBaseResolution($quantityunitUuid);
    }

    /**
     * @param string $resolutionGroup
     * @return bool
     */
    public function existQuantityUnitResolutionGroup(string $resolutionGroup): bool
    {
        return $this->quantityUnitTable->existQuantityUnitResolutionGroup($resolutionGroup);
    }
}
