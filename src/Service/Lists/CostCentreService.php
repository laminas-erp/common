<?php

namespace Lerp\Common\Service\Lists;

use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Log\Logger;
use Lerp\Common\Table\Lists\CostCentreTable;

class CostCentreService extends AbstractService
{
    /**
     * @var CostCentreTable
     */
    protected $costCentreTable;

    /**
     * @param CostCentreTable $costCentreTable
     */
    public function setCostCentreTable(CostCentreTable $costCentreTable): void
    {
        $this->costCentreTable = $costCentreTable;
    }

    public function getCostCentreIdAssoc(): array
    {
        return $this->costCentreTable->getCostCentreIdAssoc();
    }

}
