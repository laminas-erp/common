<?php

namespace Lerp\Common\Service\Lists;

use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Log\Logger;
use Lerp\Common\Table\Lists\ProjectTypeTable;

class ProjectTypeService extends AbstractService
{
    protected ProjectTypeTable $projectTypeTable;

    public function setProjectTypeTable(ProjectTypeTable $projectTypeTable): void
    {
        $this->projectTypeTable = $projectTypeTable;
    }

    /**
     * @return array All project_type ORDER BY project_type_order_priority DESC
     */
    public function getProjectTypes(): array
    {
        return $this->projectTypeTable->getProjectTypes();
    }

}
