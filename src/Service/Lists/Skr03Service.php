<?php

namespace Lerp\Common\Service\Lists;

use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Log\Logger;
use Lerp\Common\Table\Lists\Skr03Table;

class Skr03Service extends AbstractService
{
    protected Skr03Table $skr03Table;

    public function setSkr03Table(Skr03Table $skr03Table): void
    {
        $this->skr03Table = $skr03Table;
    }

    /**
     * @param bool $withoutDeposit
     * @return array
     */
    public function getSkr03s(bool $withoutDeposit = true): array
    {
        return $this->skr03Table->getSkr03s($withoutDeposit);
    }

    /**
     * @return array
     */
    public function getSkr03CodeAssoc(): array
    {
        return $this->skr03Table->getSkr03CodeAssoc();
    }
}
