<?php

namespace Lerp\Common\Service\Lists;

use Bitkorn\Trinket\Service\AbstractService;
use Lerp\Common\Table\Lists\CountryTable;
use Laminas\Log\Logger;

class CountryService extends AbstractService
{
    /**
     * @var CountryTable
     */
    protected CountryTable $countryTable;
    protected array $countryIdAssoc;

    /**
     * @param CountryTable $countryTable
     */
    public function setCountryTable(CountryTable $countryTable): void
    {
        $this->countryTable = $countryTable;
    }

    /**
     * @return array
     */
    public function getCountryUuidAssoc(): array
    {
        return $this->countryTable->getCountryIdAssoc();
    }

    public function getCountryLabel(int $countryId): string
    {
        if (!isset($this->countryIdAssoc)) {
            $this->countryIdAssoc = $this->countryTable->getCountryIdAssoc();
        }
        if (!isset($this->countryIdAssoc[$countryId])) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() No country for country_id: ' . $countryId);
        }
        return $this->countryIdAssoc[$countryId];
    }
}
