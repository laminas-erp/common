<?php

namespace Lerp\Common\Service\Lists;

use Bitkorn\Trinket\Service\AbstractService;
use Lerp\Common\Table\Lists\IndustryCategoryTable;

class IndustryCategoryService extends AbstractService
{
    protected IndustryCategoryTable $industryCategoryTable;

    public function setIndustryCategoryTable(IndustryCategoryTable $industryCategoryTable): void
    {
        $this->industryCategoryTable = $industryCategoryTable;
    }

    /**
     * @return array
     */
    public function getIndustryCategoryUuidAssoc(): array
    {
        return $this->industryCategoryTable->getIndustryCategoryUuidAssoc();
    }
}
