<?php

namespace Lerp\Common\Service\Lists;

use Bitkorn\Trinket\Service\AbstractService;
use Lerp\Common\Table\Lists\PayTermTable;

class PayTermService extends AbstractService
{
    protected PayTermTable $payTermTable;

    public function setPayTermTable(PayTermTable $payTermTable): void
    {
        $this->payTermTable = $payTermTable;
    }

    /**
     * @return array
     */
    public function getPayTerms(): array
    {
        return $this->payTermTable->getPayTerms();
    }

    /**
     * @return array
     */
    public function getPayTermCodeAssoc(): array
    {
        return $this->payTermTable->getPayTermCodeAssoc();
    }

}
