<?php

namespace Lerp\Common\Service;

use Bitkorn\Mail\Service\MailService;
use Bitkorn\Trinket\Service\AbstractService;
use Laminas\View\Resolver\TemplateMapResolver;
use Lerp\Equipment\Service\User\UserEquipService;
use Bitkorn\User\Service\Right\UserRightService;
use Psr\Container\ContainerInterface;

abstract class AbstractUnlockService extends AbstractService
{
    const RIGHT_RELEASE_PURCHASE_ORDER = 'release_purchase_order';
    const RIGHT_UNLOCK_ORDER_FINISH_REAL = 'unlock_order_finish_real';
    protected UserRightService $userRightService;
    protected UserEquipService $userEquipService;
    protected MailService $mailService;
    protected TemplateMapResolver $templateMapResolver;
    protected string $brandName;
    protected string $brandEmail;
    protected string $appUrl;

    public function setUserRightService(UserRightService $userRightService): void
    {
        $this->userRightService = $userRightService;
    }

    public function setUserEquipService(UserEquipService $userEquipService): void
    {
        $this->userEquipService = $userEquipService;
    }

    public function setMailService(MailService $mailService): void
    {
        $this->mailService = $mailService;
    }

    public function setTemplateMapResolver(TemplateMapResolver $templateMapResolver): void
    {
        $this->templateMapResolver = $templateMapResolver;
    }

    public function setBrandName(string $brandName): void
    {
        $this->brandName = $brandName;
    }

    public function setBrandEmail(string $brandEmail): void
    {
        $this->brandEmail = $brandEmail;
    }

    public function setAppUrl(string $appUrl): void
    {
        $this->appUrl = $appUrl;
    }

    public function doFactory(ContainerInterface $container): void
    {

        $this->setUserRightService($container->get(UserRightService::class));
        $this->setUserEquipService($container->get(UserEquipService::class));
        $this->setMailService($container->get(MailService::class));
        $this->setTemplateMapResolver($container->get('ViewTemplateMapResolver'));
        $config = $container->get('config');
        $this->setBrandName($config['bitkorn_user']['brand_name']);
        $this->setBrandEmail($config['bitkorn_user']['brand_email']);
        $this->setAppUrl($config['lerp_application']['app_url']);
    }

    public abstract function sendUnlockRequest(string $dataUuid, string $userUuid): bool;

    public abstract function unlockData(string $dataUuid): bool;
}
