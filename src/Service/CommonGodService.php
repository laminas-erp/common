<?php

namespace Lerp\Common\Service;

use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Db\Adapter\Adapter;
use Laminas\Log\Logger;
use Lerp\Document\Service\DocumentGodService;
use Lerp\Factoryorder\Service\FactoryorderGodService;
use Lerp\Order\Service\OrderGodService;
use Lerp\Product\Service\ProductGodService;
use Lerp\Purchase\Service\PurchaseGodService;
use Lerp\Stock\Service\StockGodService;

class CommonGodService extends AbstractService
{
    protected Adapter $adapter;
    protected DocumentGodService $documentGodService;
    protected FactoryorderGodService $factoryorderGodService;
    protected OrderGodService $orderGodService;
    protected ProductGodService $productGodService;
    protected PurchaseGodService $purchaseGodService;
    protected StockGodService $stockGodService;

    public function setAdapter(Adapter $adapter): void
    {
        $this->adapter = $adapter;
    }

    public function setDocumentGodService(DocumentGodService $documentGodService): void
    {
        $this->documentGodService = $documentGodService;
    }

    public function setFactoryorderGodService(FactoryorderGodService $factoryorderGodService): void
    {
        $this->factoryorderGodService = $factoryorderGodService;
    }

    public function setOrderGodService(OrderGodService $orderGodService): void
    {
        $this->orderGodService = $orderGodService;
    }

    public function setProductGodService(ProductGodService $productGodService): void
    {
        $this->productGodService = $productGodService;
    }

    public function setPurchaseGodService(PurchaseGodService $purchaseGodService): void
    {
        $this->purchaseGodService = $purchaseGodService;
    }

    public function setStockGodService(StockGodService $stockGodService): void
    {
        $this->stockGodService = $stockGodService;
    }

    /**
     * Delete all (in all tables) for a product.
     *
     * Delete in this order:
     * - document
     * - stock
     * - purchase
     * - factoryorder
     * - order
     * - product
     *
     * @param string $productUuid
     * @return bool
     */
    public function deleteProductDeep(string $productUuid): bool
    {
        $conn = $this->beginTransactionAdapter($this->adapter);

    }
}
