<?php

namespace Lerp\Common\Service;

use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Log\Logger;
use Lerp\Common\Table\ConfigTable;

class ConfigService extends AbstractService
{
    protected ConfigTable $configTable;
    protected array $configKeys = [];
    protected \HTMLPurifier $htmlPurifier;

    /**
     * ConfigService constructor.
     */
    public function __construct()
    {
        $this->htmlPurifier = new \HTMLPurifier();
        $this->htmlPurifier->config->set('URI.AllowedSchemes', ['data' => true]); // <img src="data:image/png;base64
    }

    public function setConfigTable(ConfigTable $configTable): void
    {
        $this->configTable = $configTable;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function existConfigKey(string $key): bool
    {
        if (!isset($this->configKeys) || empty($this->configKeys = $this->configTable->getConfigKeys())) {
            return false;
        }
        return in_array($key, $this->configKeys);
    }

    /**
     * @param string $key
     * @return string
     */
    public function getConfigValueByKey(string $key): string
    {
        if (!$this->existConfigKey($key)) {
            return '';
        }
        return $this->configTable->getConfigValueByKey($key);
    }

    /**
     * @return array ORDERed BY config_group_order_priority DESC, config_order_priority DESC
     */
    public function getConfigs(): array
    {
        return $this->configTable->getConfigs();
    }

    /**
     * @param string $key
     * @return array
     */
    public function getConfigByKey(string $key): array
    {
        return $this->configTable->getConfigByKey($key);
    }

    /**
     * @param string $key
     * @param string $value
     * @return null|string
     */
    public function filterConfigValueByValueType(string $key, string $value): ?string
    {
        if(!$this->existConfigKey($key) || empty($config = $this->getConfigByKey($key))) {
            return null;
        }
        switch($config['config_value_type']) {
            case 'int':
                return filter_var($value, FILTER_SANITIZE_NUMBER_INT) . '';
            case 'float':
                return filter_var($value, FILTER_SANITIZE_NUMBER_FLOAT) . '';
            case 'text':
                return filter_var($value, FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]) . '';
            case 'html':
                return $this->htmlPurifier->purify($value);
        }
    }

    /**
     * @param string $key
     * @param string $value
     * @return bool
     */
    public function updateConfigValue(string $key, string $value): bool
    {
        if (!$this->existConfigKey($key)) {
            return false;
        }
        return $this->configTable->updateConfigValue($key, $value) >= 0;
    }
}
