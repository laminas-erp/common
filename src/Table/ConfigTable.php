<?php

namespace Lerp\Common\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class ConfigTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'config';

    public function getConfigByKey(string $key): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['config_key' => $key]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $key
     * @return string
     */
    public function getConfigValueByKey(string $key): string
    {
        $select = $this->sql->select();
        try {
            $select->where(['config_key' => $key]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy()['config_value'];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @return array ORDERed BY config_group_order_priority DESC, config_order_priority DESC
     */
    public function getConfigs(): array
    {
        $select = $this->sql->select();
        try {
            $select->join('config_group', 'config_group.config_group_id = config.config_group_id'
                , ['config_group_label', 'config_group_desc', 'config_group_order_priority'], Select::JOIN_LEFT);
            $select->order('config_group_order_priority DESC, config_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getConfigKeys(): array
    {
        $select = $this->sql->select();
        $keys = [];
        try {
            $select->columns(['config_key']);
            $select->order('config_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $arr = $result->toArray();
                foreach ($arr as $row) {
                    $keys[] = $row['config_key'];
                }
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $keys;
    }

    public function updateConfigValue(string $key, string $value): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['config_value' => $value]);
            $update->where(['config_key' => $key]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
