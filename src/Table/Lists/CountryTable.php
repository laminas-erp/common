<?php

namespace Lerp\Common\Table\Lists;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;

class CountryTable extends AbstractLibTable
{
	/** @var string */
	protected $table = 'country';

	/**
	 * @return array
	 */
	public function getCountryIdAssoc(): array
    {
        $select = $this->sql->select();
        $uuidAssoc = [];
        try {
            $select->order('country_name ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $result = $result->toArray();
                foreach ($result as $row) {
                    $uuidAssoc[$row['country_id']] = $row['country_iso'] . ' - ' . $row['country_name'];
                }
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $uuidAssoc;
	}
}
