<?php

namespace Lerp\Common\Table\Lists;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class IndustryCategoryTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'industry_category';

    /**
     * @return array
     */
    public function getIndustryCategoryUuidAssoc(): array
    {
        $select = $this->sql->select();
        $uuidAssoc = [];
        try {
            $select->order('industry_category_label DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                do {
                    $current = $result->current();
                    $uuidAssoc[$current['industry_category_uuid']] = $current['industry_category_label'];
                    $result->next();
                } while ($result->valid());
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $uuidAssoc;
    }
}
