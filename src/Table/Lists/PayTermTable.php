<?php

namespace Lerp\Common\Table\Lists;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class PayTermTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'pay_term';

    /**
     * @param string $payTermCode
     * @return array
     */
    public function getPayTerm(string $payTermCode): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['pay_term_code' => $payTermCode]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @return array ORDERed BY pay_term_order_priority DESC
     */
    public function getPayTerms(): array
    {
        $select = $this->sql->select();
        try {
            $select->order('pay_term_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @return array
     */
    public function getPayTermCodeAssoc(): array
    {
        $select = $this->sql->select();
        $assoc = [];
        try {
            $select->order('pay_term_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $arr = $result->toArray();
                foreach ($arr as $row) {
                    $assoc[$row['pay_term_code']] = $row['pay_term_text'];
                }
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $assoc;
    }
}
