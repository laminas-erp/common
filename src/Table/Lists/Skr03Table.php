<?php

namespace Lerp\Common\Table\Lists;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class Skr03Table extends AbstractLibTable
{
    /** @var string */
    protected $table = 'skr_03';

    /**
     * @param string $skr03Code
     * @return array
     */
    public function getSkr03(string $skr03Code): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['skr_03_code' => $skr03Code]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param bool $withoutDeposit
     * @return array ORDERed BY skr_03_order_priority DESC.
     */
    public function getSkr03s(bool $withoutDeposit = true): array
    {
        $select = $this->sql->select();
        try {
            if($withoutDeposit) {
                $select->where->isNotNull('skr_03_code_deposit');
            }
            $select->order('skr_03_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getSkr03CodeAssoc(): array
    {
        $select = $this->sql->select();
        $assoc = [];
        try {
            $select->order('skr_03_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                while ($result->valid()) {
                    $c = $result->current()->getArrayCopy();
                    $assoc[$c['skr_03_code']] = $c['skr_03_desc'];
                    $result->next();
                }
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $assoc;
    }
}
