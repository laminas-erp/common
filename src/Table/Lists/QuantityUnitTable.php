<?php

namespace Lerp\Common\Table\Lists;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class QuantityUnitTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'quantityunit';

    /**
     * @param string $quantityunitUuid
     * @return array
     */
    public function getQuantityUnit(string $quantityunitUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['quantityunit_uuid' => $quantityunitUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @return array
     */
    public function getQuantityUnits(): array
    {
        $select = $this->sql->select();
        try {
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $resolutionGroup
     * @return array
     */
    public function getQuantityUnitUuidAssoc(string $resolutionGroup = ''): array
    {
        $select = $this->sql->select();
        $uuidAssoc = [];
        try {
            if ($resolutionGroup) {
                $select->where(['quantityunit_resolution_group' => $resolutionGroup]);
            }
            $select->order('quantityunit_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $result = $result->toArray();
                foreach ($result as $row) {
                    $uuidAssoc[$row['quantityunit_uuid']] = str_pad($row['quantityunit_label'], 6, '_') . '' . $row['quantityunit_name'] . '';
                }
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $uuidAssoc;
    }

    /**
     * @param string $quantityunitUuid
     * @return array
     */
    public function getQuantityUnitBaseResolution(string $quantityunitUuid): array
    {
        $select = $this->sql->select();
        try {
            $selectGroup = $this->sql->select();
            $selectGroup->columns(['quantityunit_resolution_group']);
            $selectGroup->where(['quantityunit_uuid' => $quantityunitUuid]);
            /** @var HydratingResultSet $resultG */
            $resultG = $this->selectWith($selectGroup);
            if (!$resultG->valid() || $resultG->count() != 1) {
                return [];
            }

            $select->where([
                'quantityunit_resolution' => 1,
                'quantityunit_resolution_group' => $resultG->toArray()[0]['quantityunit_resolution_group']
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $resolutionGroup
     * @return bool
     */
    public function existQuantityUnitResolutionGroup(string $resolutionGroup): bool
    {
        $select = $this->sql->select();
        try {
            $select->where(['quantityunit_resolution_group' => $resolutionGroup]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return true;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }

}
