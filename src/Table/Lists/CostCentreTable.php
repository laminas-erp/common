<?php

namespace Lerp\Common\Table\Lists;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class CostCentreTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'cost_centre';

    /**
     * @param int $costCentreId
     * @return array
     */
    public function getCostCentre(int $costCentreId): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['cost_centre_id' => $costCentreId]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getCostCentreByCode(int $costCentreCode): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['cost_centre_code' => $costCentreCode]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param int $costCentreCode
     * @return int The cost_centre_id or 0.
     */
    public function getCostCentreIdByCode(int $costCentreCode): int
    {
        if (empty($cs = $this->getCostCentreByCode($costCentreCode))) {
            return 0;
        }
        return (int)$cs['cost_centre_id'];
    }

    public function getCostCentreIdAssoc(): array
    {
        $select = $this->sql->select();
        $idAssoc = [];
        try {
            $select->order('cost_centre_code ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                do {
                    $current = $result->current();
                    $idAssoc[$current['cost_centre_id']] = $current['cost_centre_code'] . ' - ' . $current['cost_centre_label'];
                    $result->next();
                } while ($result->valid());
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $idAssoc;
    }
}
