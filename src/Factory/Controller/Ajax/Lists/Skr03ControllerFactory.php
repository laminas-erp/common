<?php

namespace Lerp\Common\Factory\Controller\Ajax\Lists;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Common\Controller\Ajax\Lists\Skr03Controller;
use Lerp\Common\Service\Lists\Skr03Service;

class Skr03ControllerFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$controller = new Skr03Controller();
		$controller->setLogger($container->get('logger'));
		$controller->setUserService($container->get(UserService::class));
        $controller->setSkr03Service($container->get(Skr03Service::class));
		return $controller;
	}
}
