<?php

namespace Lerp\Common\Factory\Controller\Ajax\Lists;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Lerp\Common\Controller\Ajax\Lists\IndustryCategoryController;
use Lerp\Common\Service\Lists\IndustryCategoryService;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class IndustryCategoryControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new IndustryCategoryController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setIndustryCategoryService($container->get(IndustryCategoryService::class));
        return $controller;
    }
}
