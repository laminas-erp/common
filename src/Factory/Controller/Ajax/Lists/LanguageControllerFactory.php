<?php

namespace Lerp\Common\Factory\Controller\Ajax\Lists;

use Bitkorn\Trinket\Table\ToolsTable;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Common\Controller\Ajax\Lists\LanguageController;

class LanguageControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new LanguageController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        /** @var ToolsTable $toolsTable */
        $toolsTable = $container->get(ToolsTable::class);
        $controller->setLangIsos($toolsTable->getEnumValuesPostgreSQL('enum_supported_lang_iso'));
        return $controller;
    }
}
