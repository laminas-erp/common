<?php

namespace Lerp\Common\Factory\Service\Lists;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Common\Service\Lists\ProjectTypeService;
use Lerp\Common\Table\Lists\ProjectTypeTable;

class ProjectTypeServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new ProjectTypeService();
        $service->setLogger($container->get('logger'));
        $service->setProjectTypeTable($container->get(ProjectTypeTable::class));
        return $service;
    }
}
