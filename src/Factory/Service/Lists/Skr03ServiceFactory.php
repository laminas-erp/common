<?php

namespace Lerp\Common\Factory\Service\Lists;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Common\Service\Lists\Skr03Service;
use Lerp\Common\Table\Lists\Skr03Table;

class Skr03ServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new Skr03Service();
        $service->setLogger($container->get('logger'));
        $service->setSkr03Table($container->get(Skr03Table::class));
        return $service;
    }
}
