<?php

namespace Lerp\Common\Factory\Service\Lists;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Common\Service\Lists\CostCentreService;
use Lerp\Common\Table\Lists\CostCentreTable;

class CostCentreServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new CostCentreService();
        $service->setLogger($container->get('logger'));
        $service->setCostCentreTable($container->get(CostCentreTable::class));
        return $service;
    }
}
