<?php

namespace Lerp\Common\Factory\Service\Lists;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Common\Service\Lists\QuantityUnitService;
use Lerp\Common\Table\Lists\QuantityUnitTable;

class QuantityUnitServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new QuantityUnitService();
        $service->setLogger($container->get('logger'));
        $service->setQuantityUnitTable($container->get(QuantityUnitTable::class));
        return $service;
    }
}
