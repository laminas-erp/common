<?php

namespace Lerp\Common\Factory\Service;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Common\Service\CommonGodService;
use Lerp\Document\Service\DocumentGodService;
use Lerp\Factoryorder\Service\FactoryorderGodService;
use Lerp\Order\Service\OrderGodService;
use Lerp\Product\Service\ProductGodService;
use Lerp\Purchase\Service\PurchaseGodService;
use Lerp\Stock\Service\StockGodService;

class CommonGodServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new CommonGodService();
        $service->setLogger($container->get('logger'));
        $service->setAdapter($container->get('dbDefault'));
        $service->setDocumentGodService($container->get(DocumentGodService::class));
        $service->setFactoryorderGodService($container->get(FactoryorderGodService::class));
        $service->setOrderGodService($container->get(OrderGodService::class));
        $service->setProductGodService($container->get(ProductGodService::class));
        $service->setPurchaseGodService($container->get(PurchaseGodService::class));
        $service->setStockGodService($container->get(StockGodService::class));
        return $service;
    }
}
