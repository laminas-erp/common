<?php

namespace Lerp\Common\Controller\Rest;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Lerp\Common\Service\Lists\CountryService;
use Laminas\Http\PhpEnvironment\Response;

class CountryListController extends AbstractUserRestController
{

    protected CountryService $countryService;

    public function setCountryService(CountryService $countryService): void
    {
        $this->countryService = $countryService;
    }

    /**
     * @OA\Get(
     *     path="/lerp-common-lists-countrylist",
     *     @OA\Response(response="200", description="A list of all countries.")
     * )
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $jsonModel->setObj($this->countryService->getCountryUuidAssoc());
        return $jsonModel;
    }

}
