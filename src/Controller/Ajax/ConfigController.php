<?php

namespace Lerp\Common\Controller\Ajax;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Lerp\Common\Service\ConfigService;

class ConfigController extends AbstractUserController
{
    protected ConfigService $configService;

    public function setConfigService(ConfigService $configService): void
    {
        $this->configService = $configService;
    }

    /**
     * @return JsonModel
     */
    public function valueByKeyAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkRightsnroles()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $jsonModel->setVal($this->configService->getConfigValueByKey($this->params('key')));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel Array with all configs ORDERed BY config_group_order_priority DESC, config_order_priority DESC
     */
    public function configsAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkRightsnroles()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $jsonModel->setArr($this->configService->getConfigs());
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    public function updateConfigValueAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkRightsnroles()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $post = $request->getPost()->toArray();
        $key = filter_var($post['key'], FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $val = $this->configService->filterConfigValueByValueType($key, $post['value']);
        if (isset($val) && $this->configService->updateConfigValue($key, $val)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}
