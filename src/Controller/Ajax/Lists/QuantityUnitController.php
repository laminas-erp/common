<?php

namespace Lerp\Common\Controller\Ajax\Lists;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Lerp\Common\Service\Lists\QuantityUnitService;
use Laminas\Http\Response;

class QuantityUnitController extends AbstractUserController
{
    protected QuantityUnitService $quantityUnitService;

    public function setQuantityUnitService(QuantityUnitService $quantityUnitService): void
    {
        $this->quantityUnitService = $quantityUnitService;
    }

    /**
     * @return JsonModel
     */
    public function uuidAssocAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkRightsnroles()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $resolutionGroup = $this->params('resol_group', '');
        if (!empty($resolutionGroup) && !$this->quantityUnitService->existQuantityUnitResolutionGroup($resolutionGroup)) {
            $resolutionGroup = '';
        }
        $jsonModel->setObj($this->quantityUnitService->getQuantityUnitUuidAssoc($resolutionGroup));
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function allAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkRightsnroles()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $jsonModel->setArr($this->quantityUnitService->getQuantityUnits());
        return $jsonModel;
    }
}
