<?php

namespace Lerp\Common\Controller\Ajax\Lists;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Lerp\Common\Service\Lists\PayTermService;

class PayTermController extends AbstractUserController
{
    protected PayTermService $payTermService;

    public function setPayTermService(PayTermService $payTermService): void
    {
        $this->payTermService = $payTermService;
    }

    /**
     * @return JsonModel
     */
    public function payTermsAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $jsonModel->setArr($this->payTermService->getPayTerms());
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}
