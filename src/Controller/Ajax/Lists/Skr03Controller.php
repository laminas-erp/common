<?php

namespace Lerp\Common\Controller\Ajax\Lists;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Lerp\Common\Service\Lists\Skr03Service;

class Skr03Controller extends AbstractUserController
{
    protected Skr03Service $skr03Service;

    public function setSkr03Service(Skr03Service $skr03Service): void
    {
        $this->skr03Service = $skr03Service;
    }

    /**
     * @return JsonModel
     */
    public function skr03sAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $jsonModel->setArr($this->skr03Service->getSkr03s($this->params()->fromQuery('without_deposit') == 'true'));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}
