<?php

namespace Lerp\Common\Controller\Ajax\Lists;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Lerp\Common\Service\Lists\CostCentreService;

class CostCentreController extends AbstractUserController
{
    protected CostCentreService $costCentreService;

    public function setCostCentreService(CostCentreService $costCentreService): void
    {
        $this->costCentreService = $costCentreService;
    }

    /**
     * @return JsonModel
     */
    public function idAssocAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $jsonModel->setObj($this->costCentreService->getCostCentreIdAssoc());
        return $jsonModel;
    }
}
