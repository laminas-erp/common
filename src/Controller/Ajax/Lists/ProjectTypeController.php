<?php

namespace Lerp\Common\Controller\Ajax\Lists;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Lerp\Common\Service\Lists\ProjectTypeService;

class ProjectTypeController extends AbstractUserController
{
    protected ProjectTypeService $projectTypeService;

    public function setProjectTypeService(ProjectTypeService $projectTypeService): void
    {
        $this->projectTypeService = $projectTypeService;
    }

    /**
     * @return JsonModel
     */
    public function projectTypesAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $jsonModel->setArr($this->projectTypeService->getProjectTypes());
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}
