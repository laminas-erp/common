<?php

namespace Lerp\Common\Controller\Ajax\Lists;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;

class LanguageController extends AbstractUserController
{
    protected array $langIsos;

    public function setLangIsos(array $langIsos): void
    {
        $this->langIsos = $langIsos;
    }

    /**
     * @return JsonModel
     */
    public function langIsosAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $jsonModel->setArr($this->langIsos);
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}
