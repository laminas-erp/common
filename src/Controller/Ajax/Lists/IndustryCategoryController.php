<?php

namespace Lerp\Common\Controller\Ajax\Lists;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Lerp\Common\Service\Lists\IndustryCategoryService;
use Laminas\Http\Response;

class IndustryCategoryController extends AbstractUserController
{
    protected IndustryCategoryService $industryCategoryService;

    public function setIndustryCategoryService(IndustryCategoryService $industryCategoryService): void
    {
        $this->industryCategoryService = $industryCategoryService;
    }

    /**
     * @return JsonModel
     */
    public function uuidAssocAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkRightsnroles()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $jsonModel->setObj($this->industryCategoryService->getIndustryCategoryUuidAssoc());
        return $jsonModel;
    }

}
